package com.kashyap.matrimony.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.kashyap.matrimony.R;
import com.kashyap.matrimony.adapter.UserListAdapter;
import com.kashyap.matrimony.database.tables.UserTbl;
import com.kashyap.matrimony.model.UserModel;
import com.kashyap.matrimony.util.Constant;

import java.util.ArrayList;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteCandidateListActivity extends BaseActivity {


    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;
    UserListAdapter userListAdapter;
    ArrayList<UserModel> userList = new ArrayList<>();
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_list);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_favorite_candidate_list), false);
        setAdapter();
        checkAndVisibleView();
    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(FavoriteCandidateListActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are You Sure Want To Delete");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deletedId = new UserTbl(FavoriteCandidateListActivity.this).deletUserById(userList.get(position).getUserId());

                    if (deletedId > 0) {
                        Toast.makeText(FavoriteCandidateListActivity.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        userListAdapter.notifyItemRemoved(position);
                        userListAdapter.notifyItemRangeChanged(0, userList.size());
                        checkAndVisibleView();
                    } else {
                        Toast.makeText(FavoriteCandidateListActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }


    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new UserTbl(this).getFavoriteUserList());
        userListAdapter = new UserListAdapter(this, userList, new UserListAdapter.OnClickListener() {
            @Override
            public void onDeleteButtonClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void onItemClick(int position) {

                Intent intent = new Intent(FavoriteCandidateListActivity.this, CandidateDetailsActivity.class);
                intent.putExtra(Constant.USER_OBJECT, userList.get(position));
                startActivity(intent);

            }

            @Override
            public void onFavoriteButton(int position) {
                int lastUpdatedUserId = new UserTbl(FavoriteCandidateListActivity.this).updateFavoriteStatus( 0 ,userList.get(position).getUserId());

                if (lastUpdatedUserId>0){
                    sendFavoriteChangeBroadcast(userList.get(position).getUserId());
                    userList.remove(position);
                    userListAdapter.notifyItemRemoved(position);
                    userListAdapter.notifyItemRangeChanged(0,userList.size());
                }

                checkAndVisibleView();

            }
        });
        rcvUserList.setAdapter(userListAdapter);
    }

    void checkAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUserList.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUserList.setVisibility(View.GONE);
        }
    }

    void sendFavoriteChangeBroadcast(int userId){
        Intent intent = new Intent(Constant.FAVORITE_CHANGE_FILTER);
        intent.putExtra(Constant.USER_ID,userId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}