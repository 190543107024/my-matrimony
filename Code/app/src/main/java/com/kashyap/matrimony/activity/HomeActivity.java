package com.kashyap.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;

import com.kashyap.matrimony.R;

import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends BaseActivity {

    @BindView(R.id.cvActAddCandidate)
    CardView cvActAddCandidate;
    @BindView(R.id.cvActListCandidate)
    CardView cvActListCandidate;
    @BindView(R.id.cvActSearchCandidate)
    CardView cvActSearchCandidate;
    @BindView(R.id.cvActFavoriteCandidate)
    CardView cvActFavoriteCandidate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_dashboard),false);
    }

    @OnClick(R.id.toolbar)
    public void onToolbarClicked() {
    }

    @OnClick(R.id.cvActAddCandidate)
    public void onCvActAddCandidateClicked() {
        Intent addCandidateIntent = new Intent(HomeActivity.this,AddCandidateActivity.class);
        startActivity(addCandidateIntent);
    }

    @OnClick(R.id.cvActListCandidate)
    public void onCvActListCandidateClicked() {
        Intent addCandidateIntent = new Intent(HomeActivity.this, CandidateListByGenderActivity.class);
        startActivity(addCandidateIntent);
    }

    @OnClick(R.id.cvActSearchCandidate)
    public void onCvActSearchCandidateClicked() {
        Intent addCandidateIntent = new Intent(HomeActivity.this,SearchCandidateActivity.class);
        startActivity(addCandidateIntent);
    }

    @OnClick(R.id.cvActFavoriteCandidate)
    public void onCvActFavoriteCandidateClicked() {
        Intent addCandidateIntent = new Intent(HomeActivity.this, FavoriteCandidateListActivity.class);
        startActivity(addCandidateIntent);
    }
}