package com.kashyap.matrimony.model;

import android.database.sqlite.SQLiteDatabase;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserCityModel implements Serializable {

    int UserCityId;
    String Name;
}
