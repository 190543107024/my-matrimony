package com.kashyap.matrimony.database.tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.util.Log;

import com.kashyap.matrimony.database.MyDatabase;
import com.kashyap.matrimony.model.UserModel;
import com.kashyap.matrimony.util.Constant;
import com.kashyap.matrimony.util.Utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.RequiresApi;

public class UserTbl extends MyDatabase {

    public static final String TABLE_NAME = "UserTbl";
    public static final String USER_ID = "UserId";
    public static final String USER_NAME = "UserName";
    public static final String NAME = "Name";
    public static final String USER_FATHER_NAME = "UserFatherName";
    public static final String USER_SURNAME = "UserSurname";
    public static final String USER_GENDER = "UserGender";
    public static final String USER_HOBBIES = "Hobbies";
    public static final String USER_DOB = "UserDob";
    public static final String USER_EMAIL_ADDRESS = "UserEmailAddress";
    public static final String USER_PHONE_NUMBER = "PhoneNumber";
    public static final String USER_LANGUAGE_ID = "UserLanguageId";
    public static final String USER_CITY_ID = "UserCityId";
    public static final String IS_FAVORITE = "IsFavorite";
    public static String AGE = "Age";
    public static final String USER_DOB_TIME = "UserDobTime";

    /* QUERY COLUMN */
    public static final String LANGUAGE = "Language";
    public static final String CITY = "City";

    public UserTbl(Context context) {
        super(context);
    }

    public UserModel getCreatedModelUsingCursor(Cursor cursor)
    {
        UserModel userModel = new UserModel();

        userModel.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        userModel.setUserCityId(cursor.getInt(cursor.getColumnIndex(USER_CITY_ID)));
        userModel.setUserLanguageId(cursor.getInt(cursor.getColumnIndex(USER_LANGUAGE_ID)));
        userModel.setUserName(cursor.getString(cursor.getColumnIndex(NAME)));
        userModel.setUserFatherName(cursor.getString(cursor.getColumnIndex(USER_FATHER_NAME)));
        userModel.setUserSurname(cursor.getString(cursor.getColumnIndex(USER_SURNAME)));
        userModel.setUserGender(cursor.getInt(cursor.getColumnIndex(USER_GENDER)));
        userModel.setUserHobbies(cursor.getString(cursor.getColumnIndex(USER_HOBBIES)));
        userModel.setUserDob(Utils.getDateToDisplay(cursor.getString(cursor.getColumnIndex(USER_DOB))));
        userModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(USER_PHONE_NUMBER)));
        userModel.setUserEmailAddress(cursor.getString(cursor.getColumnIndex(USER_EMAIL_ADDRESS)));
        userModel.setLanguage(cursor.getString(cursor.getColumnIndex(LANGUAGE)));
        userModel.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
        userModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));
        userModel.setUserDobTime(cursor.getString(cursor.getColumnIndex(USER_DOB_TIME)));

        return userModel;
    }

    public ArrayList<UserModel> getUserList()
    {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> userList = new ArrayList<>();
        String query = "SELECT " +
                " UserId," +
                " UserTbl.UserName as Name," +
                " UserFatherName," +
                " UserSurName," +
                " UserGender," +
                " UserEmailAddress," +
                " UserDob," +
                " PhoneNumber," +
                " Hobbies, " +
                " UserLanguageTbl.UserLanguageId," +
                " UserCityTbl.UserCityID," +
                " UserLanguageTbl.Name as Language," +
                " UserCityTbl.Name as City, " +
                " IsFavorite, " +
                " UserDobTime " +

                "FROM " +
                " UserTbl " +
                " INNER JOIN UserLanguageTbl ON UserTbl.UserLanguageID = UserLanguageTbl.UserLanguageID" +
                " INNER JOIN UserCityTbl ON UserTbl.UserCityID = UserCityTbl.UserCityID";
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();

        for (int i=0;i<cursor.getCount();i++)
        {
            userList.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();

        }

        cursor.close();
        db.close();

        return userList;
    }

    public UserModel getUserById(int id)
    {
        SQLiteDatabase db = getReadableDatabase();
        UserModel userModel = new UserModel();

        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + USER_ID + " = ?";
        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(id)});

        cursor.moveToFirst();
        userModel = getCreatedModelUsingCursor(cursor);

        cursor.close();;
        db.close();

        return userModel;
    }

    public ArrayList<UserModel> getUserListByGender(int gender)
    {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> userList = new ArrayList<>();

        String query = "SELECT " +
                " UserId," +
                " UserTbl.UserName as Name," +
                " UserFatherName," +
                " UserSurName," +
                " UserGender," +
                " UserEmailAddress," +
                " UserDob," +
                " PhoneNumber," +
                " Hobbies, " +
                " UserLanguageTbl.UserLanguageId," +
                " UserCityTbl.UserCityID," +
                " UserLanguageTbl.Name as Language," +
                " UserCityTbl.Name as City, " +
                " IsFavorite, "+
                " UserDobTime " +
                "FROM " +
                " UserTbl " +
                " INNER JOIN UserLanguageTbl ON UserTbl.UserLanguageID = UserLanguageTbl.UserLanguageID" +
                " INNER JOIN UserCityTbl ON UserTbl.UserCityID = UserCityTbl.UserCityID" +
                " WHERE " + USER_GENDER + " = ?";

        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(gender)});
        cursor.moveToFirst();

        for (int i=0;i<cursor.getCount();i++)
        {
            userList.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        db.close();

        return userList;
    }

    public ArrayList<UserModel> getFavoriteUserList()
    {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> userList = new ArrayList<>();

        String query = "SELECT " +
                " UserId," +
                " UserTbl.UserName as Name," +
                " UserFatherName," +
                " UserSurName," +
                " UserGender," +
                " UserEmailAddress," +
                " UserDob," +
                " PhoneNumber," +
                " Hobbies, " +
                " UserLanguageTbl.UserLanguageId," +
                " UserCityTbl.UserCityID," +
                " UserLanguageTbl.Name as Language," +
                " UserCityTbl.Name as City, " +
                " IsFavorite, "+
                " UserDobTime " +
                "FROM " +
                " UserTbl " +
                " INNER JOIN UserLanguageTbl ON UserTbl.UserLanguageID = UserLanguageTbl.UserLanguageID" +
                " INNER JOIN UserCityTbl ON UserTbl.UserCityID = UserCityTbl.UserCityID" +
                " WHERE " + IS_FAVORITE + " = ?";

        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(1)});
        cursor.moveToFirst();

        for (int i=0;i<cursor.getCount();i++)
        {
            userList.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        db.close();

        return userList;
    }



    public long insertUserById(String UserName, String UserFatherName, String UserSurname, int UserGender,
                               String UserHobbies, String UserDob, String UserEmailAddress, String PhoneNumber, int UserLanguageId,
                               int UserCityId,int IsFavorite,String UserDobTime) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(USER_NAME,UserName);
        cv.put(USER_FATHER_NAME,UserFatherName);
        cv.put(USER_SURNAME,UserSurname);
        cv.put(USER_GENDER,UserGender);
        cv.put(USER_HOBBIES,UserHobbies);
        cv.put(USER_DOB,UserDob);
        cv.put(USER_EMAIL_ADDRESS,UserEmailAddress);
        cv.put(USER_PHONE_NUMBER,PhoneNumber);
        cv.put(USER_LANGUAGE_ID,UserLanguageId);
        cv.put(USER_CITY_ID,UserCityId);
        cv.put(IS_FAVORITE,IsFavorite);
        cv.put(USER_DOB_TIME,UserDobTime);

        long lastInsertedId = db.insert(TABLE_NAME,null,cv);
        db.close();

        return lastInsertedId;
    }

    public int updateUserById(int UserId, String UserName, String UserFatherName, String UserSurname,int UserGender,
                           String UserHobbies, String UserDob, String UserEmailAddress, String PhoneNumber, int UserLanguageId,
                           int UserCityId,int IsFavorite,String UserDobTime) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(USER_NAME,UserName);
        cv.put(USER_FATHER_NAME,UserFatherName);
        cv.put(USER_SURNAME,UserSurname);
        cv.put(USER_GENDER,UserGender);
        cv.put(USER_HOBBIES,UserHobbies);
        cv.put(USER_DOB,UserDob);
        cv.put(USER_EMAIL_ADDRESS,UserEmailAddress);
        cv.put(USER_PHONE_NUMBER,PhoneNumber);
        cv.put(USER_LANGUAGE_ID,UserLanguageId);
        cv.put(USER_CITY_ID,UserCityId);
        cv.put(IS_FAVORITE,IsFavorite);
        cv.put(USER_DOB_TIME,UserDobTime);

        int lastUpdatedId = db.update(TABLE_NAME, cv,USER_ID+ " = ?",new String[]{String.valueOf(UserId)});
        db.close();

        return lastUpdatedId;
    }

    public int deletUserById(int UserId)
    {
        SQLiteDatabase db =getWritableDatabase();
        int lastDeletedId =  db.delete(TABLE_NAME,USER_ID+ " = ?",new String[]{String.valueOf(UserId)});
        db.close();

        return lastDeletedId;
    }

    public int updateFavoriteStatus(int isFavorite,int userId){

        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(IS_FAVORITE,isFavorite);
        int lastDeletedId =  db.update(TABLE_NAME,cv,USER_ID+ " = ?",new String[]{String.valueOf(userId)});
        db.close();

        return lastDeletedId;
    }
}
